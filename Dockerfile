FROM debian:latest


# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory

RUN apt-get update && apt-get install --no-install-recommends -y python3.9-minimal python3-pip
COPY requirements.txt /

RUN pip install --no-cache-dir -r /requirements.txt

# copy the content of the local src directory to the working directory
COPY app/ /app

# command to run on container start
CMD [ "python3", "/app/weatherhub.py" ]


# ref. https://community.home-assistant.io/t/migration-to-2021-7-fails-fatal-python-error-init-interp-main-cant-initialize-time/320648/10
