import os
import sys
import logging
import json
import time
import paho.mqtt.client as mqtt
import requests
import pprint
from rich import print_json
from datetime import datetime
import schedule
from os import listdir
from os.path import isfile, join
import shutil
from libhermes.configManager import  ConfigManager
from libhermes.mqttPublisher import Mqtt_publisher
from pyowm import OWM
from pyowm.utils import config
from pyowm.utils import timestamps


def translate_day_of_week(aDay: str) -> str:
    days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    if aDay == "Mon":
        return "Lundi"
    elif aDay == "Tue":
        return "Mardi"
    elif aDay == "Wed":
        return "Mercredi"
    elif aDay == "Thu":
        return "Jeudi"
    elif aDay == "Fri":
        return "Vendredi"
    elif aDay == "Sat":
        return "Samedi"
    else:
        return "Dimanche"


# ----------------------------------------------------------------------
def mqtt_send_notification(notification_message_dict: dict) -> None:
    """
    send a mqtt message to the notification topic
    :param notification_message_dict:
    :return:
    """
    if mqtt_notification_topic and mqtt_host and mqtt_port is not None:
        logger.debug(notification_message_dict)
        mqtt_client_n = mqtt.Client()
        mqtt_client_n.connect(mqtt_host, mqtt_port, 60)
        mqtt_client_n.publish(mqtt_notification_topic, json.dumps(notification_message_dict))
        mqtt_client_n.disconnect()


# ----------------------------------------------------------------------
def on_connect(client, userdata, flags, rc):
    logger.debug("MQTT connected")


# ----------------------------------------------------------------------
def degToCompass(num):
    val = int((num / 22.5) + .5)
    arr = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
    compass = arr[(val % 16)]
    if compass in ["N", "NNE"]:
        return "Nord"
    elif compass in ["NE", "ENE"]:
        return "Nord est"
    elif compass in ["E", "ESE"]:
        return "Est"
    elif compass in ["SE", "SSE"]:
        return "Sud Est"
    elif compass in ["S", "SSW"]:
        return "Sud"
    elif compass in ["SW", "WSW"]:
        return "Sud ouest"
    elif compass in ["W", "WNW"]:
        return "Ouest"
    elif compass in ["NW", "NNW"]:
        return "Nord Ouest"


# ----------------------------------------------------------------------
def on_message(client, userdata, message):
    logger.debug(f"{message.topic} : {message.payload}")
    request_type = message.payload.decode()
    if request_type in ["current", "today", "tomorrow", "j+1", "j+2", "j+3", "j+4", "weekend"]:
        logger.debug("Request for current weather")
        weather_forecast = request_openweathermap_api()
        weather_decoded_notification_message = decode_openweathermap_response(weather_forecast, request_type)
        if weather_decoded_notification_message:
            mqtt_send_notification(weather_decoded_notification_message)

    if request_type in ["xc"]:
        if paraglidable_api_key:
            logger.debug("Request for XC forecast")
            xc_forecast = request_paraglidable_api()
            crossable_days = extract_crossable_days_from_paraglidable_response(xc_forecast)
            if len(crossable_days["msg"]["xc_days"]) > 0:
                mqtt_send_notification(crossable_days)
            else:
                logger.debug("NO XC flying possibilities")
        else:
            logger.info("XC forecast not available.")


# ----------------------------------------------------------------------
def decode_openweathermap_response(a_weather_dict: dict, weather_echeance: str) -> dict:
    if weather_echeance == "current":
        current_temperature = a_weather_dict["current"]["temp"]
        wind_speed = int(a_weather_dict["current"]["wind_speed"] * 3.6)
        wind_dir = a_weather_dict["current"]["wind_deg"]
        wind_dir_compass = degToCompass(wind_dir)
        clouds = a_weather_dict["current"]["clouds"]
        weather_notification = {"channel": "weather-info"
            , "msg": {"agent": "Agent Weather"
                , "echeance": "current"
                , "city": city
                , "temperature": int(current_temperature)
                , "wind_speed": int(wind_speed)
                , "wind_dir": wind_dir_compass
                , "clouds": clouds
                }
            }
        print_json(json.dumps(a_weather_dict["current"]))
        return weather_notification

    if weather_echeance == "today":
        offset = 0
    elif weather_echeance == "tomorrow":
        offset = 1
    elif weather_echeance.lower().startswith("j+"):
        offset = int(weather_echeance[2:])

    if weather_echeance in ["today", "tomorrow", "j+1", "j+2", "j+3", "j+4"]:
        day_prev = a_weather_dict["daily"][offset]

        if weather_echeance == "tomorrow":
            day_prev = a_weather_dict["daily"][1]

        print_json(json.dumps(day_prev))
        prevision_timestamp = day_prev["dt"]
        prevision_datetime = datetime.fromtimestamp(prevision_timestamp)
        if weather_echeance in ["tomorrow", "j+1"]:
            day_of_week = "demain"
        else:
            day_of_week = translate_day_of_week(prevision_datetime.strftime('%a'))
        logger.debug(day_of_week)
        logger.debug(prevision_datetime.strftime('%a %A %d-%m-%Y, %H:%M'))
        #logger.debug(prevision_timestamp, prevision_datetime)
        temperature_day = day_prev["temp"]["day"]
        temperature_min = day_prev["temp"]["min"]
        temperature_max = day_prev["temp"]["max"]
        temperature_night = day_prev["temp"]["night"]
        wind_speed = int(day_prev["wind_speed"] * 3.6)
        wind_dir = day_prev["wind_deg"]
        wind_dir_compass = degToCompass(wind_dir)
        clouds = day_prev["clouds"]
        if "rain" in day_prev:
            rain = int(day_prev["rain"])
        else:
            rain = 0

        weather_main = day_prev["weather"][0]["main"]
        weather_descr = day_prev["weather"][0]["description"]
        logger.debug(f"temperature: min:{temperature_min}, max:{temperature_max}, nuit: {temperature_night}")
        logger.debug(f"vent: {wind_dir_compass} {wind_speed} km/h")
        logger.debug(f"{weather_descr}, couverture {clouds} %, rain {rain} mm")

        weather_notification = {"channel": "weather-info"
            , "msg": {"agent": "Agent Weather"
                , "echeance": weather_echeance
                , "day_of_week": day_of_week
                , "city": city
                , "temperature_min": int(temperature_min)
                , "temperature_max": int(temperature_max)
                , "wind_speed": int(wind_speed)
                , "wind_dir": wind_dir_compass
                , "clouds": clouds
                , "rain": rain
            }
        }
        return weather_notification
    else:
        logger.info(f"This echeance {weather_echeance} is not implemented")

        # for hourly_prev in a_weather_dict["hourly"]:
        #     logger.debug("----- hourly")
        #     prevision_timestamp = hourly_prev["dt"]
        #     prevision_datetime = datetime.fromtimestamp(prevision_timestamp)
        #     logger.debug(prevision_timestamp, prevision_datetime)
        #     temperature = hourly_prev["temp"]
        #     clouds = hourly_prev["clouds"]
        #     wind_speed = hourly_prev["wind_speed"]
        #     wind_dir = hourly_prev["wind_deg"]
        #     wind_dir_compass = degToCompass(wind_dir)
        #     weather_main = hourly_prev["weather"][0]["main"]
        #     weather_descr = hourly_prev["weather"][0]["description"]
        #     if "rain" in hourly_prev:
        #         rain_1h = hourly_prev["rain"]["1h"]
        #     else:
        #         rain_1h = 0
        #     # print_json(json.dumps(hourly_prev))
        print("========================================")


# ----------------------------------------------------------------------
def extract_crossable_days_from_paraglidable_response(a_xc_forecast: dict) -> dict:
    forecasts_by_spot = dict()

    for k, v in a_xc_forecast.items():
        for record in v:
            spot = record["name"]
            forecast = record["forecast"]
            logger.debug(f"KEY:{k} - SPOT:{spot} - FC:{forecast}")
            forecast_by_date = dict()
            if spot not in forecasts_by_spot.keys():
                forecasts_by_spot[spot] = dict()
            date_forecast = dict()
            date_forecast[k] = forecast
            forecasts_by_spot[spot][k] = forecast

    selected_days = dict()

    for spot in forecasts_by_spot.keys():
        logger.debug(f"{spot} {forecasts_by_spot[spot]}")
        for date_forecast in forecasts_by_spot[spot]:
            forecast = forecasts_by_spot[spot][date_forecast]
            fly = int(forecast["fly"] * 100)
            XC = int(forecast["XC"] * 100)
            if fly >= threshold_fly and XC >= threshold_xc:
                selected_days[spot] = {}
                selected_days[spot][date_forecast] = forecast  # {"fly": fly, "XC": XC}

    logger.debug(selected_days)

    crossable_days_notification = {"channel": "XC-info"
        , "msg": {
            "agent": "Agent Weather"
            , "xc_days": selected_days
            }
        }

    return crossable_days_notification


# ----------------------------------------------------------------------
def request_openweathermap(owm_request):
    owm_response = requests.get(owm_request)
    owm_weather = owm_response.json()
    owm_weather_display = json.dumps(owm_weather)
    print_json(owm_weather_display)
    return owm_weather


# ----------------------------------------------------------------------
def request_paraglidable_api() -> dict:
    paraglidable_request = f"https://paraglidable.com/apps/api/get.php?key={paraglidable_api_key}&format=JSON&version=1"
    response = requests.get(paraglidable_request)
    weather = response.json()
    weather_display = json.dumps(weather)
    print_json(weather_display)
    return weather


# ----------------------------------------------------------------------
def request_openweathermap_api() -> dict:
    # https://openweathermap.org/api/one-call-api
    now = datetime.now()
    # list the files in persisted folder
    forecast_files = [f for f in listdir(forecast_files_persist_path) if isfile(join(forecast_files_persist_path, f))]
    if len(forecast_files)>0:
        # there should be only one file
        last_forecast_filename = forecast_files[-1] # anyway take the last one
        last_forecast_strtime = last_forecast_filename.split(".")[0] # take the name minus extension
        last_forecast_time = datetime.strptime(last_forecast_strtime,"%Y%m%dT%H%M")
        last_forecast_age = (now - last_forecast_time).seconds
        if last_forecast_age < min_forecast_age_seconds:
            logger.debug(f"la dernière previ est recente ({forecast_files_persist_path}/{last_forecast_filename}): pas de request api")
            with open(f"{forecast_files_persist_path}/{last_forecast_filename}") as json_file:
                forecast_json = json.load(json_file)
                return forecast_json

    logger.debug("Request OWM api")
    owm_request = f"https://api.openweathermap.org/data/2.5/onecall?lat={latitude}&lon={longitude}" \
                  f"&appid={openweathermap_api_key}&units=metric&exclude=minutely&units=metric"
    weather_forecast = request_openweathermap(owm_request)
    weather_forecast_json = json.dumps(weather_forecast)

    # open file for writing, "w"
    strnow = now.strftime("%Y%m%dT%H%M")
    current_forecast_filename = f"{forecast_files_persist_path}/{strnow}.json"
    # move old forcast file in history folder
    for file in forecast_files:
        shutil.move(f"{forecast_files_persist_path}/{file}", f"{forecast_files_persist_path}/history")
    # forecast_files_persist_path
    with open(current_forecast_filename,"w") as f:
        # write json object to file
        logger.debug(f"Save forecast to {current_forecast_filename}")
        f.write(weather_forecast_json)

        weather_decoded_notification_message = decode_openweathermap_response(weather_forecast, request_type)
        if weather_decoded_notification_message:
            mqtt_send_notification(weather_decoded_notification_message)

    return weather_forecast

def weather_record_to_dict(weather_record)->dict:
    if "all" in weather_record.rain:
        rain = weather_record.rain["all"]
    elif "1h" in weather_record.rain:
        rain = weather_record.rain["1h"] *1.0
    else:
        rain = 0.0
    record = {
        "pressure": weather_record.pressure["press"],
        "clouds": weather_record.clouds,
        "rain": rain,
        "dewpoint": weather_record.dewpoint *1.0,
        "humidity": weather_record.humidity *1.0,
        #"snow": weather_record.snow,
        "temperature": weather_record.temperature('celsius')["temp"] *1.0,
        "temperature_feels_like": weather_record.temperature('celsius')["feels_like"] *1.0,
        "wind_speed": weather_record.wind()["speed"] *1.0,
        "wind_direction": weather_record.wind()["deg"]
    }
    print(record)
    return record

def get_observation():
    for location in locations:
        logger.info(f"Request weather observation for {location['name']}")
        mgr = OWM(openweathermap_api_key).weather_manager()
        res = mgr.one_call(lat=location['latitude'], lon=location['longitude'])
        observation = res.current
        to_timeserie_payload = [
            {
                "measurement": "weather",
                "tags": {"echeance": "observation", "location": location['name']},
                "fields": weather_record_to_dict(observation)
            }
        ]
        mqttPub.send_values_to_timeseries(to_timeserie_payload)

def get_forecast():
    logger.info("request weather forecast")
    mgr = OWM(openweathermap_api_key).weather_manager()
    res = mgr.one_call(lat=latitude, lon=longitude)
    hour_forecasts = res.forecast_hourly
    for hour_forecast in hour_forecasts:
        forecast_utc_timestsamp = hour_forecast.ref_time
        to_timeserie_payload = [
            {
                "measurement": "weather",
                "tags": {"echeance": "forecast"},
                "fields": weather_record_to_dict(hour_forecast),
                "time": forecast_utc_timestsamp
            }
        ]
        mqttPub.send_values_to_timeseries(to_timeserie_payload)

# ----------------------------------------------------------------------
if __name__ == "__main__":
    default_logger_format  = '%(asctime)s — %(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s'
    config_params = [
        {"name": "LOGLEVEL", "default": "INFO"},
        {"name": "LOG_FORMAT", "default": default_logger_format},
        {"name": "MQTT_HOST"},
        {"name": "MQTT_PORT", "default": 1883},
        {"name": "TOPIC_BASE_TIME_SERIE"},
        {"name": "TOPIC_BASE_STATE"},
        {"name": "TOPIC_NOTIFICATION"},
        {"name": "TOPIC_AGENT"},
        {"name": "OPENWEATHERMAP_API", "hide": True},
        {"name": "OWM_REQUEST_INTERVAL", "default": 3600},
        {"name": "DATABASE_NAME"},
        {"name": "FORECAST_PERSIST_PATH"},
        {"name": "LOCATION_FILES_PATH", "default": "/data"},
        {"name": "LOCATION_LONGITUDE"},
        {"name": "LOCATION_NAME"},
        {"name": "MIN_FORECAST_AGE_SECONDS", "default": 3600},
        {"name": "PARAGLIDABLE_API_KEY", "default": "deactivated"},
        {"name": "PARAGLIDABLE_THRESHOLD_FLY", "default": 80},
        {"name": "PARAGLIDABLE_THRESHOLD_XC", "default": 30},
    ]

    # init program
    logging.basicConfig(level=logging.DEBUG, format=default_logger_format)
    logger = logging.getLogger(__name__)
    config_manager = ConfigManager(config_params, logger)
    loglevel = config_manager.get_param("LOGLEVEL")
    logger.setLevel(loglevel)

    mqtt_host = config_manager.get_param("MQTT_HOST")
    mqtt_port = int(config_manager.get_param("MQTT_PORT"))
    mqtt_ts_topic_base = config_manager.get_param("TOPIC_BASE_TIME_SERIE")
    mqtt_topic_state = config_manager.get_param("TOPIC_BASE_STATE")
    mqtt_notification_topic = config_manager.get_param("TOPIC_NOTIFICATION")
    mqtt_agent_topic = config_manager.get_param("TOPIC_AGENT")
    # subject is the used to add an item in the state topic
    subject = config_manager.get_param("DATABASE_NAME")

    location_files_path = config_manager.get_param("LOCATION_FILES_PATH").rstrip("/")
    forecast_files_persist_path = config_manager.get_param("FORECAST_PERSIST_PATH")
    forecast_history_files_persist_path = f"{forecast_files_persist_path}/history"
    # TODO check si le dossier existe
    if not os.path.isdir(forecast_history_files_persist_path):
        os.makedirs(forecast_history_files_persist_path)

    # check locations conf files
    locations = []

    # Get the list of all files and directories
    location_files = os.listdir(location_files_path)
    for location_file in location_files:
        with open(f"{location_files_path}/{location_file}") as f:
            data = json.load(f)
            locations.append(data)

    # we won't request the api if we have yet persisted a forecast less than MIN_FORECAST_AGE_SECONDS ago
    min_forecast_age_seconds = int(config_manager.get_param("MIN_FORECAST_AGE_SECONDS"))

    openweathermap_api_key = config_manager.get_param("OPENWEATHERMAP_API")

    paraglidable_api_key = config_manager.get_param("PARAGLIDABLE_API_KEY")
    if paraglidable_api_key != "deactivated":
        threshold_fly = int(config_manager.get_param("PARAGLIDABLE_THRESHOLD_FLY"))
        threshold_xc = int(config_manager.get_param("PARAGLIDABLE_THRESHOLD_XC"))

    if config_manager.status:
        logger.info("All envvars successfully loaded")
    else:
        logger.critical(f"Some mandatory envvars are missing, the program will not start")
        sys.exit()

    topics = {}
    topics["notification"] = mqtt_notification_topic
    topics["timeseries"] = mqtt_ts_topic_base
    topics["state"] = mqtt_topic_state
    mqttPub = Mqtt_publisher(host=mqtt_host, port=mqtt_port, subject=subject, topics=topics, logger=logger)

    mqtt_client = mqtt.Client()
    mqtt_client.on_message = on_message
    mqtt_client.on_connect = on_connect
    mqtt_client.connect(mqtt_host, mqtt_port, 60)
    mqtt_client.subscribe(mqtt_agent_topic, 1)

    schedule.every(1).hours.do(get_observation)

    get_observation()
    while True:
        schedule.run_pending()
        mqtt_client.loop()
        time.sleep(0.1)
